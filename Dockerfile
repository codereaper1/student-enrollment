FROM maven:3.6.1-jdk-13-alpine AS MAVEN_BUILD

MAINTAINER Arjun Balraj

ENV BUILD_DIR=/usr/build
RUN mkdir -p $BUILD_DIR
COPY pom.xml $BUILD_DIR
COPY src $BUILD_DIR/src/
WORKDIR $BUILD_DIR

RUN mvn package -Dmaven.test.skip=true

FROM openjdk:8-jre-alpine
ENV BUILD_DIR=/usr/build
ENV APP_DIR=/usr/app
WORKDIR $APP_DIR
COPY --from=MAVEN_BUILD $BUILD_DIR/target/student-enrollment-0.0.1.jar $APP_DIR

ENV PORT 9090
EXPOSE $PORT
ENTRYPOINT ["java", "-jar", "student-enrollment-0.0.1.jar"]