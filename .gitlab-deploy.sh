#!/bin/bash

#Get servers list
set -f
string=$DEPLOY_SERVERS
array=(${string//,/ })

for i in "${!array[@]}"
do
mkdir -p ~/.ssh
echo "$DEPLOY_SERVER_PRIVATE_KEY" | tr -d '\r' > ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa
eval `ssh-agent -s`
ssh-add ~/.ssh/id_rsa
ssh-keyscan -H ${array[i]} >> ~/.ssh/known_hosts

ssh ec2-user@${array[i]} << EOF
alias aws='sudo docker run --rm -ti -v ~/.aws:/root/.aws -v $(pwd):/aws amazon/aws-cli:2.0.61'
aws --version
aws configure set aws_access_key_id $AWS_ACCESS_KEY_ID
aws configure set aws_secret_access_key $AWS_SECRET_ACCESS_KEY
aws configure set default.region $AWS_DEFAULT_REGION
echo "aws ecr get-login-password --region $AWS_DEFAULT_REGION | sudo docker login --username AWS --password-stdin $DOCKER_REGISTRY"
aws ecr get-login-password --region $AWS_DEFAULT_REGION | sudo docker login --username AWS --password-stdin $DOCKER_REGISTRY
echo "sudo docker pull $DOCKER_REGISTRY/$APP_NAME:$CI_PIPELINE_IID"
sudo docker pull $DOCKER_REGISTRY/$APP_NAME:$CI_PIPELINE_IID
echo "docker ps -q --filter name=student-enrollment-service | xargs -r sudo docker stop"
sudo docker ps -q --filter name=student-enrollment-service | xargs -r sudo docker stop
echo "docker ps -a -q -f name=student-enrollment-service | xargs -r sudo docker rm"
sudo docker ps -a -q -f name=student-enrollment-service | xargs -r sudo docker rm
echo "sudo docker run -it --rm -p 9090:9090 --name=student-enrollment-service -d $DOCKER_REGISTRY/$APP_NAME:$CI_PIPELINE_IID"
sudo docker run -it --rm -p 9090:9090 --name=student-enrollment-service -d $DOCKER_REGISTRY/$APP_NAME:$CI_PIPELINE_IID
EOF
done