# **Readme - Student Enrollment Service** #

#### **RESTful API that offers the following functions** 
* Enroll new student
* Update existing student
* Delete existing student 
* Get student info for 
	- all students
	- a specific student id
	- all students in a class
	- a specific student id and class

#### **Documentation**
    https://gitlab.com/codereaper1/student-enrollment/-/wikis/Student-Enrollment-Service

#### **Important Links**

    GitLab Project
    https://gitlab.com/codereaper1/student-enrollment

    GitLab Repo
    https://gitlab.com/codereaper1/student-enrollment/-/tree/master

    CI/CD Pipeline
    https://gitlab.com/codereaper1/student-enrollment/-/pipelines

    Application URL on AWS 
    http://ab-load-balancer-770715251.ap-east-1.elb.amazonaws.com/swagger-ui.html

    Local URL 
    http://localhost:9090/swagger-ui.html

    SonarQube
    https://sonarcloud.io/dashboard?id=com.codereaper%3Astudent-enrollment

#### **Pre-Requisites**
    * Java 13 installed
    * Apache Maven 3.6.1
    * Spring Boot
    * MySql DB (Managed service running on AWS) 

#### **How to build the application locally**
    * Please ensure that maven is installed and M2_HOME is set.
    * Clone: https://gitlab.com/codereaper1/student-enrollment.git* 
    * Execute mvn spring-boot:run from the parent folder
    * Browse the application at http://localhost:9090/swagger-ui.html

	
 