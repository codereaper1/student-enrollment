package com.codereaper.studentenrollment.api.service;

import com.codereaper.studentenrollment.api.domain.Student;
import com.codereaper.studentenrollment.api.dto.StudentDto;
import com.codereaper.studentenrollment.api.exception.StudentAlreadyExistsException;
import com.codereaper.studentenrollment.api.exception.StudentNotFoundException;
import com.codereaper.studentenrollment.api.jpa.StudentRepository;
import io.github.benas.randombeans.EnhancedRandomBuilder;
import io.github.benas.randombeans.api.EnhancedRandom;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import java.security.InvalidParameterException;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@Slf4j
public class StudentEnrollmentServiceTest {

    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private EntityManager entityManager;
    @Autowired
    private StudentEnrollmentService studentEnrollmentService;
    private EnhancedRandom enhancedRandom = EnhancedRandomBuilder.aNewEnhancedRandomBuilder()
            .seed(12345678L)
            .objectPoolSize(200)
            .stringLengthRange(4, 10)
            .collectionSizeRange(1, 20)
            .scanClasspathForConcreteTypes(true)
            .build();

    private static final Long TEST_ID_1 = 45678L;
    private static final Long TEST_ID_2 = 2345L;
    private static final Long TEST_ID_3 = 3456L;
    private static final String NATIONALITY_INDIAN = "Indian";
    private static final String NATIONALITY_BRITISH = "British";

    @Test
    public void addNewStudentForValid() {
        StudentDto studentDto = enhancedRandom.nextObject(StudentDto.class);
        studentDto.setId(TEST_ID_1);
        Long studentId = studentEnrollmentService.addNewStudent(studentDto);
        Optional<Student> byId = studentRepository.findById(studentId);
        assertNotNull(byId.get());
        assertEquals(byId.get().getId(), studentDto.getId());
        assertEquals(byId.get().getFirstName(), studentDto.getFirstName());
        assertEquals(byId.get().getLastName(), studentDto.getLastName());
        assertEquals(byId.get().getClassName(), studentDto.getClassName());
        assertEquals(byId.get().getNationality(), studentDto.getNationality());
    }

    @Test(expected = StudentNotFoundException.class)
    public void studentNotFound() {
        List<Student> students = studentEnrollmentService.getStudents(1234L, "");
        assertTrue(students.isEmpty());
    }

    @Test
    public void newlyAddedStudentFound() {
        List<Student> students = studentEnrollmentService.getStudents(TEST_ID_1, null);
        assertEquals(1, students.size());
    }

    @Test(expected = InvalidParameterException.class)
    public void firstNameIsNotProvided() {
        StudentDto studentDto = enhancedRandom.nextObject(StudentDto.class);
        studentDto.setFirstName("");
        Long id = studentEnrollmentService.addNewStudent(studentDto);
        assertNull(id);
    }

    @Test(expected = InvalidParameterException.class)
    public void classNameIsNotProvided() {
        StudentDto studentDto = enhancedRandom.nextObject(StudentDto.class);
        studentDto.setId(TEST_ID_3);
        studentDto.setClassName("");
        Long id = studentEnrollmentService.addNewStudent(studentDto);
        assertNull(id);
    }

    @Test(expected = StudentAlreadyExistsException.class)
    public void studentAlreadyExists() {
        StudentDto studentDto = enhancedRandom.nextObject(StudentDto.class);
        studentDto.setId(TEST_ID_1);
        Long id = studentEnrollmentService.addNewStudent(studentDto);
        assertNull(id);
    }

    @Test()
    public void nationalityIsNotProvided() {
        StudentDto studentDto = enhancedRandom.nextObject(StudentDto.class);
        studentDto.setNationality("");
        Long id = studentEnrollmentService.addNewStudent(studentDto);
        assertNotNull(id);
    }

    @Test
    public void updateStudentById() {
        StudentDto studentDto = enhancedRandom.nextObject(StudentDto.class);
        studentDto.setId(TEST_ID_2);
        studentDto.setNationality(NATIONALITY_INDIAN);
        studentEnrollmentService.addNewStudent(studentDto);

        StudentDto studentDtoUpdate = enhancedRandom.nextObject(StudentDto.class);
        studentDtoUpdate.setId(TEST_ID_2);
        studentDtoUpdate.setNationality(NATIONALITY_BRITISH);
        StudentDto updatedStudent = studentEnrollmentService.updateStudent(studentDtoUpdate);

        assertNotNull(updatedStudent);
        assertEquals(updatedStudent.getId(), studentDtoUpdate.getId());
        assertEquals(NATIONALITY_BRITISH, updatedStudent.getNationality());
    }

    @Test(expected = StudentNotFoundException.class)
    public void doNotUpdateStudentInfoIfNotFound() {
        StudentDto studentDto = enhancedRandom.nextObject(StudentDto.class);
        studentDto.setId(78908L);
        studentEnrollmentService.updateStudent(studentDto);
    }

    @Test(expected = StudentNotFoundException.class)
    public void deleteStudent() {
        StudentDto studentDto = enhancedRandom.nextObject(StudentDto.class);
        Long id = studentEnrollmentService.addNewStudent(studentDto);
        assertNotNull(id);
        studentEnrollmentService.deleteStudent(id);
        List<Student> students = studentEnrollmentService.getStudents(studentDto.getId(), "");
        assertEquals(0, students.size());
    }
    
    @Test
    public void isValidString() {
        assertEquals(true, ((StudentEnrollmentServiceImpl)studentEnrollmentService).isValidString("hello"));
    }

    @Test
    public void isNotValidString() {
        assertEquals(false, ((StudentEnrollmentServiceImpl)studentEnrollmentService).isValidString(""));
    }

}
