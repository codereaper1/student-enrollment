package com.codereaper.studentenrollment.api.controllers;

import com.codereaper.studentenrollment.api.controllers.StudentEnrollmentController;
import com.codereaper.studentenrollment.api.domain.Student;
import com.codereaper.studentenrollment.api.dto.StudentDto;
import com.codereaper.studentenrollment.api.service.StudentEnrollmentService;
import io.github.benas.randombeans.EnhancedRandomBuilder;
import io.github.benas.randombeans.api.EnhancedRandom;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Collections;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@ComponentScan(basePackages = "com.codereaper.studentenrollment.api")
@Slf4j
public class StudentEnrollmentControllerTest {

    @Autowired
    private MockMvc mvc;
    @Mock
    StudentEnrollmentService studentEnrollmentService;

    private EnhancedRandom enhancedRandom = EnhancedRandomBuilder.aNewEnhancedRandom();

    @Test
    public void getStudents() throws Exception {
        Mockito.when(studentEnrollmentService.getStudents(Mockito.any(), Mockito.any())).thenReturn(Collections.singletonList(enhancedRandom.nextObject(Student.class)));
        ResultActions result = mvc.perform(MockMvcRequestBuilders
                .get("/student/info"))
                .andExpect(status().isOk());
    }

    @Test
    public void addValidStudent() throws Exception {
        Mockito.when(studentEnrollmentService.addNewStudent(Mockito.any(StudentDto.class))).thenReturn(enhancedRandom.nextObject(Long.class));
        JSONObject reqBody = new JSONObject();
        reqBody.put("id", 1);
        reqBody.put("firstName", "Arjun");
        reqBody.put("lastName", "Balraj");
        reqBody.put("className", "1A");
        reqBody.put("nationality", "British");

        StudentDto input = enhancedRandom.nextObject(StudentDto.class);
        ResultActions result = mvc.perform(MockMvcRequestBuilders
                .post("/student/enroll")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(reqBody.toString()))
                .andExpect(status().isOk());
    }

    @Test
    public void addStudentWithoutFirstName() throws Exception {
        Mockito.when(studentEnrollmentService.addNewStudent(Mockito.any(StudentDto.class))).thenReturn(enhancedRandom.nextObject(Long.class));
        JSONObject reqBody = new JSONObject();
        reqBody.put("id", 1);
        reqBody.put("lastName", "Balraj");
        reqBody.put("className", "1A");
        reqBody.put("nationality", "British");
        StudentDto input = enhancedRandom.nextObject(StudentDto.class);
        ResultActions result = mvc.perform(MockMvcRequestBuilders
                .post("/student/enroll")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(reqBody.toString()))
                .andExpect(status().isInternalServerError());
    }

    @Test
    public void shouldUpdateStudentForValidData() throws Exception {
        Mockito.when(studentEnrollmentService.updateStudent(Mockito.any(StudentDto.class))).thenReturn(enhancedRandom.nextObject(StudentDto.class));
        JSONObject reqBody = new JSONObject();
        reqBody.put("id", 2);
        reqBody.put("firstName", "Aryan");
        reqBody.put("lastName", "Balraj");
        reqBody.put("className", "2A");
        reqBody.put("nationality", "Indian");
        mvc.perform(MockMvcRequestBuilders
                .post("/student/enroll")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(reqBody.toString()))
                .andExpect(status().isOk());
        JSONObject updateReq = new JSONObject();
        updateReq.put("id", 2);
        updateReq.put("firstName", "Aryan");
        updateReq.put("lastName", "Balraj");
        updateReq.put("className", "2A");
        updateReq.put("nationality", "British");
        ResultActions result1 = mvc.perform(MockMvcRequestBuilders
                .put("/student/update")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(updateReq.toString()))
                .andExpect(status().isOk());
    }

    @Test
    public void invalidStudentUpdate() throws Exception {
        Mockito.when(studentEnrollmentService.updateStudent(Mockito.any(StudentDto.class))).thenReturn(enhancedRandom.nextObject(StudentDto.class));
        JSONObject reqBody = new JSONObject();
        reqBody.put("id", 00000);
        reqBody.put("firstName", "fname");
        reqBody.put("lastName", "lname");
        reqBody.put("className", "1A");
        reqBody.put("nationality", "AIA");
        StudentDto input = enhancedRandom.nextObject(StudentDto.class);
        ResultActions result = mvc.perform(MockMvcRequestBuilders
                .put("/student/update")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(reqBody.toString()))
                .andExpect(status().isInternalServerError());
    }


    @Test
    public void deleteStudentWithValidInfo() throws Exception {
        Mockito.when(studentEnrollmentService.updateStudent(Mockito.any(StudentDto.class))).thenReturn(enhancedRandom.nextObject(StudentDto.class));
        JSONObject reqBody = new JSONObject();
        reqBody.put("id", 10011L);
        reqBody.put("firstName", "fname");
        reqBody.put("lastName", "lname");
        reqBody.put("className", "1A");
        reqBody.put("nationality", "AIA");
        StudentDto input = enhancedRandom.nextObject(StudentDto.class);
        ResultActions result = mvc.perform(MockMvcRequestBuilders
                .post("/student/enroll")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(reqBody.toString()))
                .andExpect(status().isOk());
        ResultActions result1 = mvc.perform(MockMvcRequestBuilders
                .delete("/student/delete")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content("10011"))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteStudentWithInvalidInfo() throws Exception {
        Mockito.when(studentEnrollmentService.updateStudent(Mockito.any(StudentDto.class))).thenReturn(enhancedRandom.nextObject(StudentDto.class));
        StudentDto input = enhancedRandom.nextObject(StudentDto.class);
        ResultActions result = mvc.perform(MockMvcRequestBuilders
                .delete("/student/delete")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content("12345"))
                .andExpect(status().isInternalServerError());
    }

}
