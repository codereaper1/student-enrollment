package com.codereaper.studentenrollment.api.domain;

import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@Slf4j
public class StudentTest {
    private static final String EXPECTED_TO_STRING = "Student{id=1, firstName='Arjun', lastName='Balraj', class='1A', nationality='British'}";
    @Test
    public void validToString() {
        Student student = new Student(1L, "Arjun", "Balraj", "1A", "British");
        Assert.assertEquals(EXPECTED_TO_STRING, student.toString());
    }
}
