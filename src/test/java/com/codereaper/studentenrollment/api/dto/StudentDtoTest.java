package com.codereaper.studentenrollment.api.dto;

import com.codereaper.studentenrollment.api.domain.Student;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@Slf4j
public class StudentDtoTest {
    private static final String EXPECTED_TO_STRING = "StudentDto{id=1, firstName='Arjun', lastName='Balraj', class='1A', nationality='British'}";

    @Test
    public void validToString() {
        StudentDto studentDto = new StudentDto(1L, "Arjun", "Balraj", "1A", "British");
        Assert.assertEquals(EXPECTED_TO_STRING, studentDto.toString());
    }

    @Test
    public void validCopyConstructor() {
        Student student = new Student(1L, "Arjun", "Balraj", "1A", "British");
        Assert.assertEquals(EXPECTED_TO_STRING, new StudentDto(student).toString());
    }
}
