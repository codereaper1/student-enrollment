package com.codereaper.studentenrollment.api.jpa;

import com.codereaper.studentenrollment.api.domain.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
}