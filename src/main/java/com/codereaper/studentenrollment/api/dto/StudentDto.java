package com.codereaper.studentenrollment.api.dto;

import com.codereaper.studentenrollment.api.domain.Student;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
public class StudentDto {

    @NotNull(message = "Student ID cannot be null")
    @Valid
    @ApiModelProperty(notes = "Student ID")
    private Long id;

    @ApiModelProperty(notes = "First Name")
    private String firstName;

    @ApiModelProperty(notes = "Last Name")
    private String lastName;

    @ApiModelProperty(notes = "Class")
    private String className;

    @ApiModelProperty(notes = "Nationality")
    private String nationality;

    public StudentDto(Student student) {
        id = student.getId();
        firstName = student.getFirstName();
        lastName = student.getLastName();
        className = student.getClassName();
        nationality = student.getNationality();
    }

    @Override
    public String toString() {
        return "StudentDto{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", class='" + className + '\'' +
                ", nationality='" + nationality + '\'' +
                '}';
    }
}
