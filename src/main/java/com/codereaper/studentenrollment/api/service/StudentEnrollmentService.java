package com.codereaper.studentenrollment.api.service;

import com.codereaper.studentenrollment.api.domain.Student;
import com.codereaper.studentenrollment.api.dto.StudentDto;

import java.util.List;

public interface StudentEnrollmentService {

    Long addNewStudent(StudentDto studentDto);

    List<Student> getStudents(Long id, String className);

    StudentDto updateStudent(StudentDto studentDto);

    void deleteStudent(Long id);
}
