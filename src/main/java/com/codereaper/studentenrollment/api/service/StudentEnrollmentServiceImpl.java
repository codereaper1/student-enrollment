package com.codereaper.studentenrollment.api.service;

import com.codereaper.studentenrollment.api.domain.Student;
import com.codereaper.studentenrollment.api.dto.StudentDto;
import com.codereaper.studentenrollment.api.exception.StudentAlreadyExistsException;
import com.codereaper.studentenrollment.api.exception.StudentNotFoundException;
import com.codereaper.studentenrollment.api.jpa.StudentRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class StudentEnrollmentServiceImpl implements StudentEnrollmentService {
    Logger log = LogManager.getLogger(getClass());

    private StudentRepository studentRepository;
    private final EntityManager entityManager;

    @Autowired
    public StudentEnrollmentServiceImpl(StudentRepository studentRepository, EntityManager entityManager) {
        this.studentRepository = studentRepository;
        this.entityManager = entityManager;
    }

    @Override
    public Long addNewStudent(StudentDto studentDto) {
        Optional<Student> student = studentRepository.findById(studentDto.getId());
        if (student.isPresent()) {
            throw new StudentAlreadyExistsException("Student already exists with ID: " + student.get().getId());
        }
        List<String> missingFields = new ArrayList<>();
        if (studentDto.getFirstName() == null || studentDto.getFirstName().isEmpty()) {
            missingFields.add("First Name");
            log.info("Missing Field {} ", missingFields);
        }
        if (studentDto.getClassName() == null || studentDto.getClassName().isEmpty()) {
            missingFields.add("Class");
            log.info("Missing Field {} ", missingFields);
        }
        if (!missingFields.isEmpty()) {
            log.info(missingFields);
            throw new InvalidParameterException("Required fields are missing: " + missingFields);
        }

        Student studentToAdd = new Student(studentDto.getId(),
                studentDto.getFirstName(), studentDto.getLastName(),
                studentDto.getClassName(), studentDto.getNationality());
        studentRepository.save(studentToAdd);
        log.info("Student has been enrolled : {}", studentToAdd);
        return studentToAdd.getId();
    }

    @Override
    public List<Student> getStudents(Long id, String className) {
        log.info("Query: {}", id);
        CriteriaQuery<Student> criteriaQuery = queryBuilder(className, id);
        log.info("Created SQL: {}", entityManager.createQuery(criteriaQuery).unwrap(org.hibernate.Query.class).getQueryString());
        List<Student> students = entityManager.createQuery(criteriaQuery).getResultList();
        if (students == null || students.isEmpty()) {
            throw new StudentNotFoundException("Student not found for ID " + id);
        }
        log.info("Students Fetched: {}", students);
        return students;
    }

    @Override
    public StudentDto updateStudent(StudentDto studentDto) {
        Optional<Student> studentToUpdate = studentRepository.findById(studentDto.getId());
        if (!studentToUpdate.isPresent()) {
            log.info("No data found with id: {}", studentDto.getId());
            throw new StudentNotFoundException("No data found with id: " + studentDto.getId().toString());
        } else {
            Student student = studentToUpdate.get();
            if (isValidString(studentDto.getFirstName())) {
                student.setFirstName(studentDto.getFirstName());
            }
            if (isValidString(studentDto.getLastName())) {
                student.setLastName(studentDto.getLastName());
            }
            if (isValidString(studentDto.getClassName())) {
                student.setClassName(studentDto.getClassName());
            }
            if (isValidString(studentDto.getNationality())) {
                student.setNationality(studentDto.getNationality());
            }
            studentRepository.save(student);
            log.info("Data has been updated: {}", student);
            return new StudentDto(student);
        }
    }

    @Override
    public void deleteStudent(Long id) {
        Optional<Student> forDeletion = studentRepository.findById(id);
        if (!forDeletion.isPresent()) {
            log.info("No data found with id: {}", id);
            throw new StudentNotFoundException("No data found with id: " + id);
        } else {
            Student student = forDeletion.get();
            studentRepository.delete(student);
            log.info("Student deleted with id: {}", id);
        }
    }


    private CriteriaQuery<Student> queryBuilder(String className, Long id) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Student> criteriaQuery = criteriaBuilder.createQuery(Student.class);
        Root<Student> root = criteriaQuery.from(Student.class);
        List<Predicate> predicateList = new ArrayList<>();
        if (isValidString(className)) {
            predicateList.add(criteriaBuilder.equal(root.get("className"), className));
        }
        if (id != null) {
            predicateList.add(criteriaBuilder.equal(root.get("id"), id));
        }
        criteriaQuery.where(predicateList.toArray(new Predicate[predicateList.size()]));
        return criteriaQuery;
    }

    protected boolean isValidString(String str) {
        return !"".equals(str) && null != str && !str.isEmpty();
    }

}
