package com.codereaper.studentenrollment.api.controllers;


import com.codereaper.studentenrollment.api.domain.Student;
import com.codereaper.studentenrollment.api.dto.StudentDto;
import com.codereaper.studentenrollment.api.service.StudentEnrollmentService;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@Validated
@RequestMapping("/student")
public class StudentEnrollmentController {
    Logger log = LogManager.getLogger(getClass());

    private final StudentEnrollmentService studentEnrollmentService;

    @Autowired
    public StudentEnrollmentController(StudentEnrollmentService studentEnrollmentService) {
        this.studentEnrollmentService = studentEnrollmentService;
    }

    @ApiOperation(value = "Enroll new student", response = ResponseEntity.class,
            notes = "Add a new student")
    @PostMapping(value = "/enroll", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> addNewStudent(@Valid @RequestBody StudentDto studentDto) {
        try {
            Long id = studentEnrollmentService.addNewStudent(studentDto);
            return new ResponseEntity<>("Student successfully enrolled with id : " + id, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e);
            return new ResponseEntity<>("Exception occurred while adding a new student: "
                    + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Get Student Info", response = ResponseEntity.class,
            notes = "Get Student Info based on:\n" +
            "   1. Class only\n" +
            "   2. Exact ID match\n" +
            "   3. Both class and ID\n" +
            "   4. No params -> all students")
    @GetMapping(value = "/info", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> fetchStudents(@RequestParam(value = "className", required = false) String className,
                                           @RequestParam(value = "id", required = false) Long id) {
        try {
            List<Student> studentRecord = studentEnrollmentService.getStudents(id, className);
            return new ResponseEntity<>(studentRecord, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e);
            return new ResponseEntity<>("Exception occurred while getting student info: "
                    + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Update Student Record in Database", response = ResponseEntity.class,
            notes = "Update existing student info based on ID")
    @PutMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateStudent(@Valid @RequestBody StudentDto studentDto) {
        try {
            StudentDto updated = studentEnrollmentService.updateStudent(studentDto);
            return new ResponseEntity<>(updated, HttpStatus.OK);
        } catch (Exception e) {
            log.info(e);
            return new ResponseEntity<>("Exception occurred while updating student: "
                    + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Delete Student Record from Database", response = ResponseEntity.class,
            notes = "Deelete an existing student based on ID")
    @DeleteMapping(value = "/delete", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteStudent(@Valid @RequestBody Long studentId) {
        try {
            studentEnrollmentService.deleteStudent(studentId);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            log.info(e);
            return new ResponseEntity<>("Exception occurred while deleting student: "
                    + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}